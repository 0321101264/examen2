from django.urls import path
from . import views
from .views import (
    UserProfileListView,
    UserProfileDetailView,
    UserProfileCreateView,
    UserProfileUpdateView,
    UserProfileDeleteView,
    ToDoCreateView,
     ToDoListView,
     ToDoUpdateView,
     ToDoDeleteView,
     ToDoDetailView
)

app_name = 'todo_api_client'

urlpatterns = [
    path('profiles/', UserProfileListView.as_view(), name='userprofile_list'),
    path('profiles/<int:pk>/', UserProfileDetailView.as_view(), name='userprofile_detail'),
    path('profiles/create/', UserProfileCreateView.as_view(), name='userprofile_create'),
    path('profiles/<int:pk>/update/', UserProfileUpdateView.as_view(), name='userprofile_update'),
    path('profiles/<int:pk>/delete/', UserProfileDeleteView.as_view(), name='userprofile_delete'),


path('todos/create/', ToDoCreateView.as_view(), name='todo_create'),
path('todos/', ToDoListView.as_view(), name='list_todo'),
path('todos/<int:pk>/', ToDoDetailView.as_view(), name='todo_detail'),
path('todos/<int:pk>/update/', ToDoUpdateView.as_view(), name='todo_update'),
path('todos/<int:pk>/delete/', ToDoDeleteView.as_view(), name='todo_delete'),



    # URLs para la lista de todos los pendientes
    path('todos/ids/', views.todo_list_ids, name='todo_list_ids'),
    path('todos/ids-titles/', views.todo_list_ids_titles, name='todo_list_ids_titles'),
    path('todos/unresolved/', views.todo_list_unresolved, name='todo_list_unresolved'),
    path('todos/resolved/', views.todo_list_resolved, name='todo_list_resolved'),
    path('todos/ids-user/', views.todo_list_ids_user, name='todo_list_ids_user'),
    path('todos/resolved-user/', views.todo_list_resolved_user, name='todo_list_resolved_user'),
    path('todos/unresolved-user/', views.todo_list_unresolved_user, name='todo_list_unresolved_user'),


]
