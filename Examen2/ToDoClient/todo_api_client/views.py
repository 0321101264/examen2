# todo_api_client/views.py

from django.shortcuts import render, redirect
from django.views.generic import TemplateView
import requests
from django.views import generic
from .forms import CreateUserProfileForm, UpdateUserProfileForm 
from django.http import JsonResponse



class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Ejemplo de solicitud GET para obtener la lista de tareas desde la API
        response = requests.get('http://localhost:8000/todo/todos/')
        if response.status_code == 200:
            tasks = response.json()
            context['tasks'] = tasks
        return context

class UserProfileListView(generic.View):
    template_name = "profile_list.html"
    context = {}
    url = "http://127.0.0.1:8000/todo/profiles/"

    def get(self, request):
        response = requests.get(self.url)
        profiles = response.json() if response.status_code == 200 else []
        self.context = {
            "profiles": profiles
        }
        return render(request, self.template_name, self.context)

class UserProfileDetailView(generic.View):
    template_name = "profile_detail.html"
    context = {}
    url = "http://127.0.0.1:8000/todo/profiles/"
    
    def get(self, request, pk):
        response = requests.get(f"{self.url}{pk}/")
        profile = response.json() if response.status_code == 200 else None
        self.context = {
            "profile": profile
        }
        return render(request, self.template_name, self.context)
    

class UserProfileCreateView(generic.View):
    template_name = "profile_create.html"
    url =  "http://127.0.0.1:8000/todo/profiles/"

    def get(self, request):
        form = CreateUserProfileForm()  # Inicializar el formulario vacío
        context = {
            "form": form
        }
        return render(request, self.template_name, context)

    def post(self, request):
        form = CreateUserProfileForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            response = requests.post(self.url, json=data)
            if response.status_code == 201:  # 201 Created
                return redirect('todo_api_client:userprofile_list')  # Redirige a la lista de GrantGoals u otra URL de éxito
            else:
                context = {
                    "form": form,
                    "error": f"Failed to create GrantGoal: {response.content.decode('utf-8')}"
                }
                return render(request, self.template_name, context, status=response.status_code)
        else:
            context = {
                "form": form,
                "error": "Invalid form data"
            }
            return render(request, self.template_name, context)


from django.shortcuts import redirect, render
from django.http import JsonResponse
from django.views import View
import requests
from .forms import UpdateUserProfileForm  # Asegúrate de importar el formulario adecuado

class UserProfileUpdateView(View):
    template_name = "profile_update.html"
    url_detail = "http://127.0.0.1:8000/todo/profiles/{pk}/"
    url_update = "http://127.0.0.1:8000/todo/profiles/{pk}/update/"

    def get(self, request, pk):
        detail_url = self.url_detail.format(pk=pk)
        response = requests.get(detail_url)
        if response.status_code == 200:
            data = response.json()
            form = UpdateUserProfileForm(initial=data)
            context = {"form": form, "profile_id": pk}
            return render(request, self.template_name, context)
        else:
            return JsonResponse({'error': 'UserProfile not found'}, status=404)

    def post(self, request, pk):
        form = UpdateUserProfileForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            update_url = self.url_update.format(pk=pk)
            response = requests.put(update_url, json=data)
            if response.status_code == 200:
                return redirect('todo_api_client:userprofile_list')
            else:
                context = {"form": form, "error": "Failed to update profile"}
                return render(request, self.template_name, context, status=response.status_code)
        else:
            context = {"form": form, "profile_id": pk}
            return render(request, self.template_name, context)

from django.shortcuts import redirect
from django.http import JsonResponse
from django.views import View  # Importa la clase View de django.views
import requests

class UserProfileDeleteView(View):
    template_name = "profile_delete.html"
    url_detail = "http://127.0.0.1:8000/todo/profiles/{pk}/"
    url_delete = "http://127.0.0.1:8000/todo/profiles/{pk}/delete/"

    def get(self, request, pk):
        detail_url = self.url_detail.format(pk=pk)
        response = requests.get(detail_url)
        if response.status_code == 200:
            data = response.json()
            context = {
                "object": data
            }
            return render(request, self.template_name, context)
        else:
            return JsonResponse({'error': 'UserProfile not found'}, status=404)

    def post(self, request, pk):
        delete_url = self.url_delete.format(pk=pk)
        response = requests.delete(delete_url)
        if response.status_code == 204:
            return redirect('todo_api_client:userprofile_list')
        else:
            return JsonResponse({'error': 'Failed to delete profile'}, status=response.status_code)
        
from .forms import CreateToDoForm

class ToDoCreateView(View):
    template_name = "todos_create.html"
    url = "http://127.0.0.1:8000/todo/todos/"

    def get(self, request):
        form = CreateToDoForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = CreateToDoForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # Ensure 'assigned_user' is an integer (ID)
            data['assigned_user'] = int(data['assigned_user'])
            response = requests.post(self.url, json=data)
            if response.status_code == 201:
                return redirect('todo_api_client:list_todo')
            else:
                return render(request, self.template_name, {'form': form, 'error': response.content})
        return render(request, self.template_name, {'form': form})

class ToDoListView(View):
    template_name = "todos_list.html"
    url = "http://127.0.0.1:8000/todo/todos/"

    def get(self, request):
        response = requests.get(self.url)
        if response.status_code == 200:
            todos = response.json()
            return render(request, self.template_name, {'todos': todos})
        else:
            return render(request, self.template_name, {'error': 'Failed to fetch tasks'})


from django.shortcuts import render, redirect
from django.views import generic
import requests
from .forms import ToDoUpdateForm

class ToDoDetailView(generic.View):
    template_name = "detail_todo.html"
    url = "http://127.0.0.1:8000/todo/todos/"

    def get(self, request, pk):
        detail_url = f"{self.url}{pk}/"
        response = requests.get(detail_url)
        if response.status_code == 200:
            data = response.json()
            context = {"todo": data}
            return render(request, self.template_name, context)
        else:
            return render(request, 'error.html', {'error': 'ToDo not found'}, status=response.status_code)

class ToDoUpdateView(generic.View):
    template_name = "update_todo.html"
    url = "http://127.0.0.1:8000/todo/todos/"

    def get(self, request, pk):
        detail_url = f"{self.url}{pk}/"
        response = requests.get(detail_url)
        if response.status_code == 200:
            data = response.json()
            form = ToDoUpdateForm(initial=data)
            context = {
                "form": form,
                "todo_id": pk
            }
            return render(request, self.template_name, context)
        else:
            return render(request, 'error.html', {'error': 'ToDo not found'}, status=response.status_code)

    def post(self, request, pk):
        update_url = f"{self.url}{pk}/update/"
        form = ToDoUpdateForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            # Convertir el campo 'deadline' a string antes de enviar la solicitud JSON
            data['deadline'] = data['deadline'].strftime('%Y-%m-%d') if data['deadline'] else None
            response = requests.put(update_url, json=data)
            if response.status_code == 200:
                return redirect('todo_api_client:todo_detail', pk=pk)
            else:
                context = {
                    "form": form,
                    "todo_id": pk,
                    "error": "Failed to update ToDo"
                }
                return render(request, self.template_name, context, status=response.status_code)
        else:
            context = {
                "form": form,
                "todo_id": pk,
                "error": "Invalid form data"
            }
            return render(request, self.template_name, context)

class ToDoDeleteView(generic.View):
    template_name = "delete_todo.html"
    url = "http://127.0.0.1:8000/todo/todos/"

    def get(self, request, pk):
        detail_url = f"{self.url}{pk}/"
        response = requests.get(detail_url)
        if response.status_code == 200:
            data = response.json()
            context = {
                "todo": data
            }
            return render(request, self.template_name, context)
        else:
            return render(request, 'error.html', {'error': 'ToDo not found'}, status=response.status_code)

    def post(self, request, pk):
        delete_url = f"{self.url}{pk}/delete/"
        response = requests.delete(delete_url)
        if response.status_code == 204:
            return redirect('todo_api_client:list_todo')
        else:
            return render(request, self.template_name, {'error': 'Failed to delete ToDo'}, status=response.status_code)



# views.py

from django.shortcuts import render
import requests

def todo_list_ids(request):
    url = 'http://127.0.0.1:8000/todo/todos/ids/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_ids.html', {'todos': todos})

def todo_list_ids_titles(request):
    url = 'http://127.0.0.1:8000/todo/todos/ids-titles/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_ids_titles.html', {'todos': todos})

def todo_list_unresolved(request):
    url = 'http://127.0.0.1:8000/todo/todos/unresolved/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_unresolved.html', {'todos': todos})

def todo_list_resolved(request):
    url = 'http://127.0.0.1:8000/todo/todos/resolved/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_resolved.html', {'todos': todos})

def todo_list_ids_user(request):
    url = 'http://127.0.0.1:8000/todo/todos/ids-user/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_ids_user.html', {'todos': todos})

def todo_list_resolved_user(request):
    url = 'http://127.0.0.1:8000/todo/todos/resolved-user/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_resolved_user.html', {'todos': todos})

def todo_list_unresolved_user(request):
    url = 'http://127.0.0.1:8000/todo/todos/unresolved-user/'
    response = requests.get(url)
    todos = response.json()
    return render(request, 'todo_list_unresolved_user.html', {'todos': todos})
