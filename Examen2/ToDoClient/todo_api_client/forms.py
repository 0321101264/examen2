from django import forms

class CreateUserProfileForm(forms.Form):
    first_name = forms.CharField(max_length=255, required=False)
    last_name = forms.CharField(max_length=255, required=False)
    email = forms.EmailField()
    area = forms.CharField(max_length=255)

class UpdateUserProfileForm(forms.Form):
    first_name = forms.CharField(max_length=255, required=False)
    last_name = forms.CharField(max_length=255, required=False)
    email = forms.EmailField()
    area = forms.CharField(max_length=255)

import requests
class CreateToDoForm(forms.Form):
    task_title = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    task_description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), required=False)
    assigned_user = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}))
    duration_days = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}), initial=7)
    task_priority = forms.ChoiceField(choices=[
        ("HIGH", "High"),
        ("MEDIUM", "Medium"),
        ("LOW", "Low"),
    ], widget=forms.Select(attrs={'class': 'form-control'}))
    task_status = forms.ChoiceField(choices=[
        ("NOT_STARTED", "Not Started"),
        ("IN_PROGRESS", "In Progress"),
        ("COMPLETED", "Completed"),
    ], widget=forms.Select(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        super(CreateToDoForm, self).__init__(*args, **kwargs)
        # Populate the assigned_user choices with data from the API
        response = requests.get("http://127.0.0.1:8000/todo/profiles/")
        if response.status_code == 200:
            users = response.json()
            self.fields['assigned_user'].choices = [(user['id'], user['email']) for user in users]


class ToDoUpdateForm(forms.Form):
    task_title = forms.CharField(max_length=255, widget=forms.TextInput(attrs={'class': 'form-control'}))
    task_description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), required=False)
    is_completed = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}), required=False)
    assigned_user = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}))
    duration_days = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}), initial=7)
    deadline = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control'}), required=False)
    task_progress = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}), required=False)
    task_priority = forms.ChoiceField(choices=[
        ("HIGH", "High"),
        ("MEDIUM", "Medium"),
        ("LOW", "Low"),
    ], widget=forms.Select(attrs={'class': 'form-control'}))
    task_status = forms.ChoiceField(choices=[
        ("NOT_STARTED", "Not Started"),
        ("IN_PROGRESS", "In Progress"),
        ("COMPLETED", "Completed"),
    ], widget=forms.Select(attrs={'class': 'form-control'}))
    is_active = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}), required=False)

    def __init__(self, *args, **kwargs):
        super(ToDoUpdateForm, self).__init__(*args, **kwargs)
        # Populate the assigned_user choices with data from the API
        response = requests.get("http://127.0.0.1:8000/todo/profiles/")
        if response.status_code == 200:
            users = response.json()
            self.fields['assigned_user'].choices = [(user['id'], user['email']) for user in users]