from rest_framework import generics
from .models import ToDo
from .serializers import ToDoSerializer
from rest_framework.response import Response
from .models import UserProfile
import requests
from .serializers import ListUsers,DetailUsers,DeleteUsers,UpdateUsers
from .models import UserProfile
from .serializers import (
    TodoIdSerializer, TodoIdTitleSerializer,
    TodoUnresolvedSerializer, TodoResolvedSerializer,
    TodoIdUserIdSerializer, TodoResolvedUserIdSerializer,
    TodoUnresolvedUserIdSerializer
)
class TodoListIdsView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = TodoIdSerializer

class TodoListIdsTitlesView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = TodoIdTitleSerializer

class TodoListUnresolvedView(generics.ListAPIView):
    serializer_class = TodoUnresolvedSerializer

    def get_queryset(self):
        return ToDo.objects.filter(task_status='NOT_STARTED')

class TodoListResolvedView(generics.ListAPIView):
    serializer_class = TodoResolvedSerializer

    def get_queryset(self):
        return ToDo.objects.filter(task_status='COMPLETED')

class TodoListIdsUserIdView(generics.ListAPIView):
    queryset = ToDo.objects.all()
    serializer_class = TodoIdUserIdSerializer

class TodoListResolvedUserIdView(generics.ListAPIView):
    serializer_class = TodoResolvedUserIdSerializer

    def get_queryset(self):
        return ToDo.objects.filter(task_status='COMPLETED')

class TodoListUnresolvedUserIdView(generics.ListAPIView):
    serializer_class = TodoUnresolvedUserIdSerializer

    def get_queryset(self):
        return ToDo.objects.filter(task_status='NOT_STARTED')

class UserProfileListCreate(generics.ListCreateAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = ListUsers

class UserProfileRetrieve(generics.RetrieveAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = DetailUsers

class UserProfileUpdate(generics.RetrieveUpdateAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UpdateUsers

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

class UserProfileDelete(generics.DestroyAPIView):
    queryset = UserProfile.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=204)


class ToDoCreateAPIView(generics.CreateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer

class ToDoRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer

class ToDoDestroyAPIView(generics.DestroyAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer


from rest_framework import generics
from .models import ToDo
from .serializers import (
    ToDoSerializer,
    CreateToDoSerializer,
    UpdateToDoSerializer,
    DetailToDoSerializer,
    DeleteToDoSerializer
)

class ToDoListCreateView(generics.ListCreateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreateToDoSerializer
        return ToDoSerializer

class ToDoRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = DetailToDoSerializer

    def get_serializer_class(self):
        if self.request.method in ['PUT', 'PATCH']:
            return UpdateToDoSerializer
        return DetailToDoSerializer

class ToDoDestroyAPIView(generics.DestroyAPIView):
    queryset = ToDo.objects.all()
    serializer_class = DeleteToDoSerializer

class ToDoCreateAPIView(generics.CreateAPIView):
    queryset = ToDo.objects.all()
    serializer_class = CreateToDoSerializer




