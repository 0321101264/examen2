from rest_framework import serializers
from .models import ToDo
from .models import UserProfile
from django.contrib.auth.models import User




class ToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = '__all__'

class TodoIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id']

class TodoIdTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'task_title']

class TodoUnresolvedSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'task_title']

class TodoResolvedSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = ['id', 'task_title']

class TodoIdUserIdSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField(source='assigned_user.id')

    class Meta:
        model = ToDo
        fields = ['id', 'user_id']

class TodoResolvedUserIdSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField(source='assigned_user.id')

    class Meta:
        model = ToDo
        fields = ['id', 'user_id']

class TodoUnresolvedUserIdSerializer(serializers.ModelSerializer):
    user_id = serializers.ReadOnlyField(source='assigned_user.id')

    class Meta:
        model = ToDo
        fields = ['id', 'user_id']
    

class ListUsers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = [
            'id', 'first_name', 'last_name', 'email', 'area'
        ]


class DetailUsers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = "__all__"


class CreateUsers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = [
            'id', 'first_name', 'last_name', 'area', 'email'
        ]


class UpdateUsers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = "__all__"


class DeleteUsers(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = "__all__"



from rest_framework import serializers
from .models import ToDo

class ToDoSerializer(serializers.ModelSerializer):
    assigned_user = serializers.SerializerMethodField()

    class Meta:
        model = ToDo
        fields = '__all__'

    def get_assigned_user(self, obj):
        return {
            'id': obj.assigned_user.id,
            'first_name': obj.assigned_user.first_name,
            'last_name': obj.assigned_user.last_name,
            'email': obj.assigned_user.email,
            'area': obj.assigned_user.area
        }

class CreateToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = [
            'task_title',
            'task_description',
            'assigned_user',
            'duration_days',
            'task_priority',
            'task_status'
        ]

class UpdateToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = [
            'task_title',
            'task_description',
            'is_completed',
            'assigned_user',
            'duration_days',
            'deadline',
            'task_progress',
            'task_priority',
            'task_status',
            'is_active'
        ]

class DetailToDoSerializer(serializers.ModelSerializer):
    assigned_user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = ToDo
        fields = '__all__'

class DeleteToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ToDo
        fields = '__all__'
