from django.urls import path
from . import views

urlpatterns = [

    ####PENDIENTES
    path('todos/ids/', views.TodoListIdsView.as_view(), name='todo-list-ids'),
    path('todos/ids-titles/', views.TodoListIdsTitlesView.as_view(), name='todo-list-ids-titles'),
    path('todos/unresolved/', views.TodoListUnresolvedView.as_view(), name='todo-list-unresolved'),
    path('todos/resolved/', views.TodoListResolvedView.as_view(), name='todo-list-resolved'),
    path('todos/ids-user/', views.TodoListIdsUserIdView.as_view(), name='todo-list-ids-user'),
    path('todos/resolved-user/', views.TodoListResolvedUserIdView.as_view(), name='todo-list-resolved-user'),
    path('todos/unresolved-user/', views.TodoListUnresolvedUserIdView.as_view(), name='todo-list-unresolved-user'),


    #### CRUD USUARIOS

    path('profiles/', views.UserProfileListCreate.as_view(), name='user-profile-list-create'),
    path('profiles/<int:pk>/', views.UserProfileRetrieve.as_view(), name='user-profile-retrieve'),
    path('profiles/<int:pk>/update/', views.UserProfileUpdate.as_view(), name='user-profile-update'),
    path('profiles/<int:pk>/delete/', views.UserProfileDelete.as_view(), name='user-profile-destroy'),


    ####CRUD TAREAS

    path('todos/', views.ToDoListCreateView.as_view(), name='list_todo'),
    path('todos/create/', views.ToDoCreateAPIView.as_view(), name='todo_create'),
    path('todos/<int:pk>/update/', views.ToDoRetrieveUpdateAPIView.as_view(), name='todo_update'),
    path('todos/<int:pk>/', views.ToDoRetrieveUpdateAPIView.as_view(), name='todo_detail'),  # Detalle y Actualización
    path('todos/<int:pk>/delete/', views.ToDoDestroyAPIView.as_view(), name='todo_delete'),


]
