from django.db import models
from django.contrib import admin
from datetime import timedelta, datetime
from django.db import models
from django.contrib.auth.models import User



AREA_CHOICES = [
        ('HR', 'Human Resources'),
        ('IT', 'Information Technology'),
        ('SALES', 'Sales'),
        ('FINANCE', 'Finance'),
        ('OPS', 'Operations'),
    ]


class UserProfile(models.Model):
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    area = models.TextField(blank=True)
    email = models.EmailField(blank=True)
    


CHOICES_PRIORITY = (
    ("HIGH", "High"),
    ("MEDIUM", "Medium"),
    ("LOW", "Low"),
)

CHOICES_STATUS = (
    ("NOT_STARTED", "Not Started"),
    ("IN_PROGRESS", "In Progress"),
    ("COMPLETED", "Completed"),
)

class ToDo(models.Model):
    task_title = models.CharField(max_length=255)
    task_description = models.TextField(blank=True)
    is_completed = models.BooleanField(default=False)
    assigned_user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    duration_days = models.IntegerField(default=7)
    deadline = models.DateField(blank=True, null=True)
    task_progress = models.FloatField(default=0.0)
    task_priority = models.CharField(max_length=8, choices=CHOICES_PRIORITY)
    task_status = models.CharField(max_length=16, choices=CHOICES_STATUS)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.task_title

    def save(self, *args, **kwargs):
        if not self.deadline:
            self.deadline = datetime.now().date() + timedelta(days=self.duration_days)
        super().save(*args, **kwargs)


admin.site.register(UserProfile)
admin.site.register(ToDo)
